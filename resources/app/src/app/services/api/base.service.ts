import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BaseService {
  public http = inject(HttpClient);
  public baseUrl = environment.baseUrl;
  public apiUrl = environment.apiUrl;
}
